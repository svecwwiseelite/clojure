(defn say-hello
         [name]
         (println (str "Hello, " name)))

(defn say-hello
         "Takes name argument and say hello to the name"
         [name]
         (println (str "Hello, " name)))
         

(doc say-hello)
;;use doc to read documentation