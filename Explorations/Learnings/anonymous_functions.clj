;;#() is the shortcut for fn.
(fn [] (println "Hello world"))

(def hello-world-func (fn [] (println "Hello world")))
 
(hello-world-func)

#(+ 1 1)

#(+ 1 %)

(let [plus-numbers #(+ 1 %1 %2 %3)]

;;% will be replaced with arguments passed to the function. 
When the function takes multiple arguments, %1 is for the first argument, %2 is for the second and so on.