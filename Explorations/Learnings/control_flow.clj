 (if true
         (println "This is always printed")
         (println "This is never printed"))
         
(defn positive-number [numbers]
         (if-let [pos-nums (not-empty (filter pos? numbers))]
           pos-nums
           "no positive numbers"))
(when true
         (println "one")
         (println "two"))

(when-let [pos-nums (filter pos? [ -1 -2 1 2])]
          pos-nums
          (println "one")
          (println "two"))

(defn case-test-1
         [n]
         (case n
            1 "n is 1"
            2 "n is 2"
            "n is other"))     

(defn cond-test
         [n]
         (cond
           (= n 1) "n is 1"
           (and (> n 3) (< n 10)) "n is over 3 and under 10"
           :else "n is other"))
            