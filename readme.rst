**Introduction:**

Clojure is a functional programming language. It provides the tools to avoid mutable state, provides functions as first-class objects, and emphasizes recursive iteration instead of side-effect based looping. Clojure is impure, in that it doesn’t force your program to be referentially transparent, and doesn’t strive for 'provable' programs. The philosophy behind Clojure is that most parts of most programs should be functional and that programs that are more functional are more robust.

**Features:**

* Dynamic Development
* Functional Programming
* List In Stupid Parenthesis
* Runtime Polymorphism
* Concurrent Programming

**Pre-requisites:**

* Ensure that sudo package is installed.

**Installation:**

* sudo apt-get update -y
* sudo apt-get install -y clojure


**To check the version:**

* (clojure-version)

**Resources**

* `Good hub of info from Kyle Cordes <http://learn-clojure.com/>`_ 
* `Clojure Documentation <http://clojuredocs.org/ and make it better>`_
* `Clojure cheatsheet from Steve Tayon <http://clojure.org/cheatsheet>`_
